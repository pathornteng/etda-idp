require('dotenv').config();
module.exports = {
  port: 3002 || process.env.PORT,
  baseUrl: "http://etda-idp.wisered.com" || process.env.BASE_URL,
  version: "0.1.0",
  serverKey: 'server.key',
  scope: ['openid', 'profile_kyc', 'profile'],
  auth: {
    db: 'local',
    twoFactor: false,
  },
  ldap: {
    url: 'ldap://10.2.2.3:389',
    baseDN: 'ou=main,dc=etda,dc=or,dc=th',
    filter: '(&(objectclass=user)(userPrincipalName=*))',
  },
  mode: 'test',
}
