'use strict'

const User = require('./models/user');

const user = new User({
  username: 'alice',
  password: '1234',
  firstname: 'Alice',
  lastname: 'Wonderland',
  email: 'alice@etda.or.th',
  nationalId: '110145333231',
  passportNumber: 'A145335',
});

async function main() {
  await user.save();
}

main();
